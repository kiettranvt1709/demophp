<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::group([
//     'prefix' => 'auth'
// ], function () {
//     Route::post('login', 'auth/AuthController@login');
//     Route::post('signup', 'auth/AuthController@signup');
  
//     Route::group([
//       'middleware' => 'auth:api'
//     ], function() {
//         Route::get('logout', 'auth/AuthController@logout');
//         Route::get('user', 'auth/AuthController@user');
//     });
// });

Route::post('login', 'Auth\UserController@login');
Route::post('register', 'Auth\UserController@register');
Route::group(['middleware' => 'auth:api'], function()
{
   Route::post('details', 'Auth\UserController@details');
});

